//
//  FirstInteractor.swift
//  ViperTest
//
//  Created by Dmitriy Melnichenko on 23/11/2016.
//  Copyright © 2016 mozydev. All rights reserved.
//

// MARK: - FirstInteractor

final class FirstInteractor {

    // MARK: - VIPER stack

    weak var presenter: FirstInteractorOutput!

    // MARK: -

}

// MARK: - FirstInteractorInput

extension FirstInteractor: FirstInteractorInput {

}
