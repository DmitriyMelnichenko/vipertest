//
//  FirstRouter.swift
//  ViperTest
//
//  Created by Dmitriy Melnichenko on 23/11/2016.
//  Copyright © 2016 mozydev. All rights reserved.
//

import Perform
import Chamomile
import UIKit

// MARK: - FirstRouter

final class FirstRouter {

    // MARK: - VIPER stack

    weak var flowController: FlowController!

}

// MARK: - FirstRouterInput

extension FirstRouter: FirstRouterInput {
    func openSecondModule(text: String, secondModuleOutput: SecondModuleOutput) {
        flowController.openModule(using: .openSecond) {
            guard let moduleInput = $0 as? SecondModuleInput else { fatalError() }
            moduleInput.displayText(text: text)
            
            return secondModuleOutput
        }
    }
}

extension Segue {
    
    
    static var openSecond: Segue<SecondViewController> {
        return .init(identifier: "transitionToNext")
    }
    
}
