//
//  FirstAssember.swift
//  ViperTest
//
//  Created by Dmitriy Melnichenko on 23/11/2016.
//  Copyright © 2016 mozydev. All rights reserved.
//

import Foundation

// MARK: - FirstAssembler

final class FirstAssembler: NSObject {

    @IBOutlet weak var view: FirstViewController!

    override func awakeFromNib() {
        super.awakeFromNib()

        let interactor = FirstInteractor()
        let presenter = FirstPresenter()
        let router = FirstRouter()

        view.presenter = presenter
        view.moduleInput = presenter
        interactor.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        router.flowController = view
    }

}
