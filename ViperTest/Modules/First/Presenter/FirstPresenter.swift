//
//  FirstPresenter.swift
//  ViperTest
//
//  Created by Dmitriy Melnichenko on 23/11/2016.
//  Copyright © 2016 mozydev. All rights reserved.
//

import Chamomile

// MARK: - FirstPresenter

final class FirstPresenter {

    // MARK: - VIPER stack

    weak var moduleOutput: ModuleOutput?
    weak var view: FirstViewInput!
    var interactor: FirstInteractorInput!
    var router: FirstRouterInput!

}

// MARK: - FirstViewOutput

extension FirstPresenter: FirstViewOutput {
    func handleNextTap(text: String) {
        router.openSecondModule(text: text, secondModuleOutput: self)
    }
}

// MARK: - FirstInteractorOutput

extension FirstPresenter: FirstInteractorOutput {

}

// MARK: - FirstModuleInput

extension FirstPresenter: FirstModuleInput {

}

extension FirstPresenter: SecondModuleOutput {
    func displayLabelText(text: String) {
        view.displayLabelText(text: text)
    }
}
