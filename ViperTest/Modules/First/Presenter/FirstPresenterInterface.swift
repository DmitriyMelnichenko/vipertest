//
//  FirstPresenterInterface.swift
//  ViperTest
//
//  Created by Dmitriy Melnichenko on 23/11/2016.
//  Copyright © 2016 mozydev. All rights reserved.
//

import Chamomile

// MARK: - FirstViewOutput

protocol FirstViewOutput: class {
    func handleNextTap(text:String)
}

// MARK: - FirstInteractorOutput

protocol FirstInteractorOutput: class {

}

// MARK: - FirstModuleInput

protocol FirstModuleInput: ModuleInput {

}

// MARK: - FirstModuleOutput

protocol FirstModuleOutput: ModuleOutput {

}
