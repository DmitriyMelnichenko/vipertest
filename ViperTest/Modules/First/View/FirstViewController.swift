//
//  FirstViewController.swift
//  ViperTest
//
//  Created by Dmitriy Melnichenko on 23/11/2016.
//  Copyright © 2016 mozydev. All rights reserved.
//

import Chamomile

// MARK: - FirstViewController

final class FirstViewController: UIViewController, FlowController {

    // MARK: - VIPER stack

    var presenter: FirstViewOutput!
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var label: UILabel!
    
    
    @IBAction func receiveNextTap(_ sender: AnyObject) {
        presenter.handleNextTap(text: textField.text!)
    }
    

}

// MARK: - FirstViewInput

extension FirstViewController: FirstViewInput {
    func displayTextInField(text: String) {
        textField.text = text
    }
    
    func displayLabelText(text: String) {
        label.text = text
    }
}
