//
//  SecondRouter.swift
//  ViperTest
//
//  Created by Dmitriy Melnichenko on 23/11/2016.
//  Copyright © 2016 mozydev. All rights reserved.
//

import Chamomile

// MARK: - SecondRouter

final class SecondRouter {

    // MARK: - VIPER stack

    weak var flowController: FlowController!

}

// MARK: - SecondRouterInput

extension SecondRouter: SecondRouterInput {
    func returnToFirst(text: String, moduleOutput: SecondModuleOutput) {
        moduleOutput.displayLabelText(text: text)
    }
}
