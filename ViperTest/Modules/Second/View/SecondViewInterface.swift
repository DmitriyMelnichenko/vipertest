//
//  SecondViewInterface.swift
//  ViperTest
//
//  Created by Dmitriy Melnichenko on 23/11/2016.
//  Copyright © 2016 mozydev. All rights reserved.
//

// MARK: - SecondViewInput

protocol SecondViewInput: class {
    func displayText(text: String)
    

}
