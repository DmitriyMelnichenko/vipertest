//
//  SecondViewController.swift
//  ViperTest
//
//  Created by Dmitriy Melnichenko on 23/11/2016.
//  Copyright © 2016 mozydev. All rights reserved.
//

import Chamomile

// MARK: - SecondViewController

final class SecondViewController: UIViewController, FlowController {

    // MARK: - VIPER stack

    var presenter: SecondViewOutput!

    @IBOutlet weak var label: UILabel!
    
    // MARK: - Life cycle
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter.handleViewWillDisappear(text: label.text!)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.handleViewDidLoad()
    }

}

// MARK: - SecondViewInput

extension SecondViewController: SecondViewInput {
    func displayText(text: String) {
        label.text = text
    }
    
}
