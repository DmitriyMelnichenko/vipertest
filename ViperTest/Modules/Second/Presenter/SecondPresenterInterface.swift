//
//  SecondPresenterInterface.swift
//  ViperTest
//
//  Created by Dmitriy Melnichenko on 23/11/2016.
//  Copyright © 2016 mozydev. All rights reserved.
//

import Chamomile

// MARK: - SecondViewOutput

protocol SecondViewOutput: class {
    func handleViewDidLoad()
    func handleViewWillDisappear(text: String)
}

// MARK: - SecondInteractorOutput

protocol SecondInteractorOutput: class {

}

// MARK: - SecondModuleInput

protocol SecondModuleInput: ModuleInput {
    func displayText(text: String)
}

// MARK: - SecondModuleOutput

protocol SecondModuleOutput: ModuleOutput {
    func displayLabelText(text: String)
}

