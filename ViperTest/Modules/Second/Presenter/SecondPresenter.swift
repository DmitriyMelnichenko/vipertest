//
//  SecondPresenter.swift
//  ViperTest
//
//  Created by Dmitriy Melnichenko on 23/11/2016.
//  Copyright © 2016 mozydev. All rights reserved.
//

import Chamomile

// MARK: - SecondPresenter

final class SecondPresenter {

    // MARK: - VIPER stack

    weak var moduleOutput: ModuleOutput?
    weak var view: SecondViewInput!
    var interactor: SecondInteractorInput!
    var router: SecondRouterInput!
    var text: String!

}

// MARK: - SecondViewOutput

extension SecondPresenter: SecondViewOutput {
    func handleViewDidLoad() {
        view.displayText(text: self.text)
    }
    
    func handleViewWillDisappear(text: String) {
        let secondModuleOutput = moduleOutput as! SecondModuleOutput
        router.returnToFirst(text: text, moduleOutput: secondModuleOutput)
    }
}

// MARK: - SecondInteractorOutput

extension SecondPresenter: SecondInteractorOutput {

}

// MARK: - SecondModuleInput

extension SecondPresenter: SecondModuleInput {
    func displayText(text: String) {
        self.text = text
    }
}
