//
//  SecondAssember.swift
//  ViperTest
//
//  Created by Dmitriy Melnichenko on 23/11/2016.
//  Copyright © 2016 mozydev. All rights reserved.
//

import Foundation

// MARK: - SecondAssembler

final class SecondAssembler: NSObject {

    @IBOutlet weak var view: SecondViewController!

    override func awakeFromNib() {
        super.awakeFromNib()

        let interactor = SecondInteractor()
        let presenter = SecondPresenter()
        let router = SecondRouter()

        view.presenter = presenter
        view.moduleInput = presenter
        interactor.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        router.flowController = view
    }

}
