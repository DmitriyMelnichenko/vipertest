//
//  SecondInteractor.swift
//  ViperTest
//
//  Created by Dmitriy Melnichenko on 23/11/2016.
//  Copyright © 2016 mozydev. All rights reserved.
//

// MARK: - SecondInteractor

final class SecondInteractor {

    // MARK: - VIPER stack

    weak var presenter: SecondInteractorOutput!

    // MARK: -

}

// MARK: - SecondInteractorInput

extension SecondInteractor: SecondInteractorInput {

}
